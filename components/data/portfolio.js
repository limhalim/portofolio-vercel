const portfolio = [
  {
    id: "01",
    title: "UI App Qur'an",
    img: "/images/portfolio-01.png",
    category: "Mobile App",
    keyword: ["Mobile", "App", "UI-UX"],
    liveUrl: "https://dribbble.com/shots/16972260-UI-App-Qur-an",
  },

  {
    id: "02",
    title: "Study App Design",
    img: "/images/portfolio-02.png",
    category: "Mobile App",
    keyword: ["Mobile", "App", "UI-UX"],
    liveUrl: "https://dribbble.com/shots/16840464-Study-App-Design",
  },

  {
    id: "03",
    title: "Food App UI",
    img: "/images/portfolio-03.png",
    category: "Mobile App",
    keyword: ["Mobile", "App", "UI-UX"],
    liveUrl: "https://dribbble.com/shots/16796200-Food-App",
  },

  {
    id: "04",
    title: "UI Design News App",
    img: "/images/portfolio-04.png",
    category: "Mobile App",
    keyword: ["Mobile", "App", "UI-UX"],
    liveUrl: "https://dribbble.com/shots/16703949-UI-Design-News-App",
  },

  {
    id: "05",
    title: "Ui Design iMovie App ",
    img: "/images/portfolio-05.jpeg",
    category: "Mobile App",
    keyword: ["Mobile", "App", "UI-UX"],
    liveUrl:
      "https://dribbble.com/shots/15631481-IMovie-create-your-be-fun-and-happy",
  },

  {
    id: "06",
    title: "Travel Design",
    img: "/images/portfolio-06.png",
    category: "Mobile App",
    keyword: ["Mobile", "App", "UI-UX"],
    liveUrl: "https://dribbble.com/shots/14131930-Tour-World",
  },

  {
    id: "07",
    title: "Duplicate Spotify UI Web",
    img: "/images/portfolio-07.png",
    category: "Web Design",
    keyword: ["Web", "Web design", "UI-UX"],
    liveUrl: "https://dribbble.com/shots/16752290-UI-Design-Spotify",
  },

  {
    id: "08",
    title: "UI Design Web IDN ",
    img: "/images/portfolio-08.png",
    category: "Web Design",
    keyword: ["Web", "Web design", "UI-UX"],
    liveUrl: "https://dribbble.com/shots/16971018-UI-Web-IDN",
  },
];

export default portfolio;
