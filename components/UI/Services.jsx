import React from "react";
import { Container, Row, Col } from "reactstrap";
import SectionSubtitle from "./SectionSubtitle";
import classes from "../../styles/services.module.css";
import ServicesItem from "./ServicesItem";

const Services = () => {
  return (
    <section id="services">
      <Container>
        <Row>
          <Col lg="6" md="6">
            <div className={`${classes.services__container}`}>
              <div>
                <ServicesItem
                  title="UI/UX Designer"
                  icon="ri-layout-masonry-line"
                />

                <ServicesItem
                  title="Database Developer"
                  icon="ri-database-2-line"
                />
              </div>

              <ServicesItem
                title="Front-end Developer"
                icon="ri-code-s-slash-line"
              />
            </div>
          </Col>

          <Col lg="6" md="6" className={`${classes.service__title}`}>
            <SectionSubtitle subtitle="" />
            <h3 className="mb-0 mt-4">Better Design,</h3>
            <h3 className="mb-4">Better Experience</h3>
            <p>
              I'm a Developer proficient in React JS framework and using
              bootstrap or tailwind as css style. In design, I also design
              applications, websites, posters, and many more that I can do in
              design. And in database proficient on MySQL and MongoDB to be a
              database developer.
            </p>
          </Col>
        </Row>
      </Container>
    </section>
  );
};

export default Services;
